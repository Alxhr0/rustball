use rand::Rng;
use cli_input::prelude::*;
use colored::*;

const ANSWERS: &[&str] = &["Yes", "No", "Maybe", "Ask me later", "It is certain", "Reply hazy, try again", "Don’t count on it", "My sources say no", "My reply is no", "Outlook not so good", "Very doubtful", "Cannot predict now", "Concentrate and ask again", "As I see it, yes", "Most likely", "Outlook good", "Signs point to yes", "Segmentation Fault(core dumped)", "Ask someone else or don't, I don't care.", "Fuck off", "Stfu", "I'm playing Minecraft not now", "I'm behind you", "Idiot", "Nuh uh", "Yuh uh", "Guess what, I don't care"];

fn main() {
    let random_index = rand::thread_rng().gen_range(0..ANSWERS.len());
    let random_answer = ANSWERS[random_index];

    let _question = input!("{}: ", "Question".bold());

    println!("{}", random_answer.bold());

    
}
